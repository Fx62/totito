// Variables globales
var currentPlayer = "X";
var board = ["", "", "", "", "", "", "", "", ""];

// Función para cambiar el jugador actual
function changePlayer() {
  if (currentPlayer === "X") {
    currentPlayer = "O";
  } else {
    currentPlayer = "X";
  }
}

// Función para verificar si hay un ganador
function checkWinner() {
  // Combinaciones ganadoras
  var winningCombos = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];

  // Verificar cada combinación ganadora
  for (var i = 0; i < winningCombos.length; i++) {
    var combo = winningCombos[i];
    if (board[combo[0]] === currentPlayer && board[combo[1]] === currentPlayer && board[combo[2]] === currentPlayer) {
      return true;
    }
  }

  return false;
}

// Función para manejar el clic en una celda
function cellClick(e) {
  // Obtener el ID de la celda clickeada
  var cellId = e.target.id;

  // Verificar si la celda ya está ocupada o si ya hay un ganador
  if (board[cellId] !== "" || checkWinner()) {
    return;
  }

  // Actualizar la celda con el jugador actual
  board[cellId] = currentPlayer;
  e.target.textContent = currentPlayer;

  // Verificar si hay un ganador
  if (checkWinner()) {
    alert("¡" + currentPlayer + " ha ganado!");
    return;
  }

  // Cambiar al siguiente jugador
  changePlayer();
}

// Función para reiniciar el juego
function resetGame() {
  // Reiniciar las variables globales
  currentPlayer = "X";
  board = ["", "", "", "", "", "", "", "", ""];

  // Reiniciar la interfaz de usuario
  var cells = document.getElementsByTagName("td");
  for (var i = 0; i < cells.length; i++) {
    cells[i].textContent = "";
  }
}

// Agregar manejadores de eventos
var cells = document.getElementsByTagName("td");
for (var i = 0; i < cells.length; i++) {
  cells[i].addEventListener("click", cellClick);
}

var resetButton = document.getElementById("reset");
resetButton.addEventListener("click", resetGame);

